package com.example.demo.dto;

import com.example.demo.security.entity.Authority;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    Long id;
    String name;
    String surname;
    String image;
    boolean approved;
    List<Authority> authorities;
}
