package com.example.demo.config;


import com.example.demo.entity.Activity;
import com.example.demo.entity.Admin;
import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.repository.ActivityRepository;
import com.example.demo.repository.AdminRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.repository.TeacherRepository;
import com.example.demo.security.entity.Authority;
import com.example.demo.security.entity.AuthorityName;
import com.example.demo.security.entity.User;
import com.example.demo.security.repository.AuthorityRepository;
import com.example.demo.security.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;

@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    TeacherRepository teacherRepository;
    @Autowired
    ActivityRepository activityRepository;
    @Autowired
    AuthorityRepository authorityRepository;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    UserRepository userRepository;
    Calendar.Builder calenderBuilder = new Calendar.Builder();

    @Transactional
    @Override
    public void run(ApplicationArguments args) throws Exception {
        //STUDENT
        Student student1 = Student.builder()
                .studentId("123456789")
                .name("Somruk")
                .surname("Laothamjinda")
                .image("https://www.googleapis.com/download/storage/v1/b/imageuploader-fc55d.appspot.com/o/2563-05-02%20161344719-get_image_hi.jpg?generation=1588410832446668&alt=media")
                .build();
        Student student2 = Student.builder()
                .studentId("987654321")
                .name("John")
                .surname("Smith")
                .image("https://www.googleapis.com/download/storage/v1/b/imageuploader-fc55d.appspot.com/o/2563-05-02%20161629218-pexels-photo-428364.jpeg?generation=1588410997472066&alt=media")
                .build();
        Student student3 = Student.builder()
                .studentId("123123123")
                .name("Tommy")
                .surname("Jones")
                .image("https://www.googleapis.com/download/storage/v1/b/imageuploader-fc55d.appspot.com/o/2563-05-02%20161831404-man-in-black-crew-neck-shirt-3992435.jpg?generation=1588411121520193&alt=media")
                .build();

        //TEACHER
        Teacher teacher1 = Teacher.builder()
                .name("Jayakrit")
                .surname("Hirisajja")
                .image("https://www.googleapis.com/download/storage/v1/b/imageuploader-fc55d.appspot.com/o/2563-05-02%20163908658-14203212_10154247579430743_1800150409250303150_n.jpg?generation=1588412356502508&alt=media")
                .build();
        Teacher teacher2 = Teacher.builder()
                .name("Dave")
                .surname("Doe")
                .image("https://www.googleapis.com/download/storage/v1/b/imageuploader-fc55d.appspot.com/o/2563-05-02%20162233022-freestock_93658960.jpg?generation=1588411380712808&alt=media")
                .build();
        //ADMIN
        Admin admin = Admin.builder()
                .name("Admin.")
                .surname("Dto")
                .build();
        //ACTIVITY
        Activity activity1 = Activity.builder()
                .name("CAMT Eating Contest")
                .location("CAMT 113")
                .description("Let invite CAMT students to eat")
                .host(teacher1)
                .maxEnrolledStudent(30)
                .hourCredit(10)
                .academicYear(2020)
                .semester(1)
                .build();
        Activity activity2 = Activity.builder()
                .name("Do Nothing And Pass")
                .location("CAMT 113")
                .description("Don't do anything, just join and you will pass")
                .host(teacher1)
                .maxEnrolledStudent(1)
                .hourCredit(50)
                .academicYear(2020)
                .semester(2)
                .build();
        Activity activity3 = Activity.builder()
                .name("Sleeping at Camt")
                .location("CAMT 114")
                .description("You can come to sleep to get activity credit")
                .host(teacher2)
                .maxEnrolledStudent(20)
                .hourCredit(40)
                .academicYear(2021)
                .semester(1)
                .build();
        Activity activity4 = Activity.builder()
                .name("Cleaning Canal")
                .location("CAMT Parking Lot")
                .description("Get dirty and get tired")
                .host(teacher2)
                .maxEnrolledStudent(70)
                .hourCredit(10)
                .academicYear(2021)
                .semester(1)
                .build();

        teacher1.getActivities().add(activity1);
        teacher1.getActivities().add(activity2);
        teacher2.getActivities().add(activity3);
        teacher2.getActivities().add(activity4);
        activity1 = activityRepository.save(activity1);
        activity2 = activityRepository.save(activity2);
        activity3 = activityRepository.save(activity3);
        activity4 = activityRepository.save(activity4);
        //AUTHORITY
        Authority authStudent, authLecturer, authAdmin;
        authStudent = Authority.builder().name(AuthorityName.ROLE_STUDENT).build();
        authLecturer = Authority.builder().name(AuthorityName.ROLE_TEACHER).build();
        authAdmin = Authority.builder().name(AuthorityName.ROLE_ADMIN).build();
        authStudent = authorityRepository.save(authStudent);
        authLecturer = authorityRepository.save(authLecturer);
        authAdmin = authorityRepository.save(authAdmin);
        //USER
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        User adminUser, studentUser1,studentUser2,studentUser3,teacherUser1, teacherUser2;
        adminUser = User.builder()
                .username("admin@admin.com")
                .password(encoder.encode("admin"))
                .firstname("Admin")
                .lastname("Admin")
                .email("admin@admin.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        adminUser.getAuthorities().add(authAdmin);
        studentUser1 = User.builder()
                .username("student1@elearning.cmu.ac.th")
                .password(encoder.encode("s1password"))
                .firstname("Somruk")
                .lastname("Laothamjinda")
                .email("student1@elearning.cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        studentUser2 = User.builder()
                .username("student2@elearning.cmu.ac.th")
                .password(encoder.encode("s2password"))
                .firstname("John")
                .lastname("Smith")
                .email("student2@elearning.cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        studentUser3 = User.builder()
                .username("student3@elearning.cmu.ac.th")
                .password(encoder.encode("s3password"))
                .firstname("Tommy")
                .lastname("Jones")
                .email("student3@elearning.cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        studentUser1.getAuthorities().add(authStudent);
        studentUser2.getAuthorities().add(authStudent);
        studentUser3.getAuthorities().add(authStudent);
        teacherUser1 = User.builder()
                .username("teacher1@elearning.cmu.ac.th")
                .password(encoder.encode("t1password"))
                .firstname("Jayakrit")
                .lastname("Hirisajja")
                .email("teacher1@elearning.cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        teacherUser2 = User.builder()
                .username("teacher2@elearning.cmu.ac.th")
                .password(encoder.encode("t2password"))
                .firstname("Dave")
                .lastname("Doe")
                .email("teacher2@elearning.cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        teacherUser1.getAuthorities().add(authLecturer);
        teacherUser2.getAuthorities().add(authLecturer);

        student1.setUser(studentUser1);
        studentUser1.setAppUser(student1);
        student2.setUser(studentUser2);
        studentUser2.setAppUser(student2);
        student3.setUser(studentUser3);
        studentUser3.setAppUser(student3);
        admin.setUser(adminUser);
        adminUser.setAppUser(admin);
        teacher1.setUser(teacherUser1);
        teacherUser1.setAppUser(teacher1);
        teacher2.setUser(teacherUser2);
        teacherUser2.setAppUser(teacher2);

        //Save into UserRepository
        studentUser1 = userRepository.save( studentUser1);
        studentUser2 = userRepository.save( studentUser2);
        studentUser3 = userRepository.save( studentUser3);
        adminUser = userRepository.save(adminUser );
        teacherUser1 = userRepository.save(teacherUser1);
        teacherUser2 = userRepository.save(teacherUser2);

        //Save into respective role repository
        student1 = studentRepository.save(student1);
        student2 = studentRepository.save(student2);
        student3 = studentRepository.save(student3);
        admin = adminRepository.save(admin);
        teacher1 = teacherRepository.save(teacher1);
        teacher2 = teacherRepository.save(teacher2);

    }


}

