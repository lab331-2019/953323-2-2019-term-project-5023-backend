package com.example.demo.service;

import com.example.demo.dao.ActivityDao;
import com.example.demo.entity.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    ActivityDao activityDao;
    @Override
    public List<Activity> getAllActivities() {
        return activityDao.getAllActivities();
    }

    @Override
    public Activity findById(Long id) {
        return activityDao.findById(id);
    }

    @Override
    public Activity deleteById(Long id) {
        return activityDao.deleteById(id);
    }

    @Override
    public Activity saveActivity(Activity activity) {
        return activityDao.saveActivity(activity);
    }
}
