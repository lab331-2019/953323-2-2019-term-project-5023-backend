package com.example.demo.dao;

import com.example.demo.entity.Activity;
import com.example.demo.repository.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ActivityDaoImpl implements ActivityDao {
    @Autowired
    ActivityRepository activityRepository;

    @Override
    public List<Activity> getAllActivities() {
        return activityRepository.findAll();
    }
    
    @Override
    public Activity findById(Long id) {
        return activityRepository.findById(id).orElse(null);
    }

    @Override
    public Activity deleteById(Long id) {
        Activity activity = activityRepository.findById(id).orElse(null);
        activityRepository.deleteById(id);
        return activity;
    }

    @Override
    public Activity saveActivity(Activity activity) {
        return activityRepository.save(activity);
    }
}
