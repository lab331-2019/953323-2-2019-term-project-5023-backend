package com.example.demo.util;

import com.example.demo.dto.*;
import com.example.demo.entity.Activity;
import com.example.demo.entity.Admin;
import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.security.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;


import java.util.List;

@Mapper
public interface MapperUtil {
    MapperUtil INSTANCE = Mappers.getMapper( MapperUtil.class );

    @Mappings({})
    TeacherDto getTeacherDto(Teacher teacher);

    @Mappings({})
    List<TeacherDto> getTeacherDto(List<Teacher> teachers);

    //TODO: CONSIDER DELETE THIS
    @Mappings({})
    AdminDto getAdminDto(Admin admin);
    //TODO: CONSIDER DELETE THIS
    @Mappings({})
    List<AdminDto> getAdminDto(List<Admin> admins);

    @Mappings({})
    StudentDto getStudentDto(Student student);

    @Mappings({})
    List<StudentDto> getStudentDto(List<Student> students);

    @Mappings({})
    ActivityDto getActivityDto(Activity activity);

    @Mappings({})
    List<ActivityDto> getActivityDto(List<Activity> activities);

    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities")
    })
    UserDto getUserDto(Student student);
    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities")
    })
    UserDto getUserDto(Teacher teacher);
    //TODO: CONSIDER DELETE THIS
    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities")
    })
    UserDto getUserDto(Admin admin);
    @Mappings({
            @Mapping(target = "password", source = "user.password"),
            @Mapping(target = "email", source = "user.username"),
    })
    StudentProfileDto getStudentProfileDto(Student student);
    @Mappings({

    })
    StudentActivityDto getStudentActivityDto(Student student);

    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities"),
            @Mapping(target = "name", source = "user.appUser.name"),
            @Mapping(target = "surname", source = "user.appUser.surname"),
            @Mapping(target = "image", source = "user.appUser.image")
    })
    UserDto getUserDto(User user);
}
