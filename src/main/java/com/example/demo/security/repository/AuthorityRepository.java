package com.example.demo.security.repository;

import com.example.demo.security.entity.Authority;
import com.example.demo.security.entity.AuthorityName;
import org.springframework.data.repository.CrudRepository;

public interface AuthorityRepository extends CrudRepository<Authority,Long> {
    Authority findByName(AuthorityName input);
}
