package com.example.demo.repository;

import com.example.demo.entity.Teacher;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeacherRepository extends CrudRepository<Teacher,Long> {
    List<Teacher> findAll();
    List<Teacher> findByNameIgnoreCaseContaining(String name);
    List<Teacher> findBySurnameIgnoreCaseContaining(String name);

}
