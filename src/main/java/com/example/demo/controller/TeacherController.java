package com.example.demo.controller;

import com.example.demo.entity.Teacher;
import com.example.demo.service.TeacherService;
import com.example.demo.util.MapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class TeacherController {
    @Autowired
    TeacherService teacherService;

    @GetMapping("/teachers")
    public ResponseEntity getAllTeachers() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getTeacherDto(teacherService.getAllTeachers()));
    }
    @GetMapping("/teachers/{id}")
    public ResponseEntity getTeacherById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getTeacherDto(teacherService.findById(id)));
    }
    @DeleteMapping("/teachers/{id}")
    public ResponseEntity<?> deleteTeacherById(@PathVariable long id) {
        return ResponseEntity.ok(teacherService.deleteById(id));
    }
    @PostMapping("/teachers")
    public ResponseEntity saveTeacher(@RequestBody Teacher teacher) {
        return ResponseEntity.ok(teacherService.saveTeacher(teacher));
    }
}

