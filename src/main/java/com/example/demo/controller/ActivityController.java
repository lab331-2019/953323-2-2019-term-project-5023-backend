package com.example.demo.controller;

import com.example.demo.dto.ActivityDto;
import com.example.demo.entity.Activity;
import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.service.ActivityService;
import com.example.demo.service.StudentService;
import com.example.demo.service.TeacherService;
import com.example.demo.util.MapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ActivityController {
    @Autowired
    ActivityService activityService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    StudentService studentService;
    @GetMapping("/activities")
    public ResponseEntity getAllActivities() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.getAllActivities()));
    }
    @GetMapping("/activities/{id}")
    public ResponseEntity getActivityById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.findById(id)));
    }
    @DeleteMapping("/activities/{id}")
    public ResponseEntity<?> deleteActivityById(@PathVariable Long id) {
        return ResponseEntity.ok(activityService.deleteById(id));
    }
    @CrossOrigin
    @PostMapping("/activities")
    public ResponseEntity saveActivity(@RequestBody Activity activity) {
        System.out.println(activity.getHost().getId());
        Teacher teacher = teacherService.findById(activity.getHost().getId());
        System.out.println(teacher.getId());
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
    }
    @CrossOrigin
    @PutMapping("/activities/{id}")
    public ResponseEntity updateActivity(@PathVariable("id") Long id, @RequestBody ActivityDto activityDto) {
        System.out.println(activityDto.getHost().getId());
        Teacher teacher = teacherService.findById(activityDto.getHost().getId());
        Activity currentAct = activityService.findById(activityDto.getId());
        currentAct.setDescription(activityDto.getDescription());
        currentAct.setLocation(activityDto.getLocation());
        currentAct.setName(activityDto.getName());

        currentAct.setMaxEnrolledStudent((activityDto.getMaxEnrolledStudent()));
        currentAct.setHourCredit((activityDto.getHourCredit()));
        currentAct.setSemester(activityDto.getSemester());
        currentAct.setAcademicYear(activityDto.getAcademicYear());
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(currentAct)));
    }
    @CrossOrigin
    @GetMapping("/activities/{id}/enroll/{stdId}")
    public ResponseEntity enrollActivity(@PathVariable("id") Long id, @PathVariable("stdId") Long stdId) {
        Activity activity = activityService.findById(id);
        Student student = studentService.findById(stdId);
        student.getEnrolledActivities().add(activity);
        activity.getEnrolledStudents().add(student);
        activityService.saveActivity(activity);
        studentService.saveStudent(student);
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
    }

    private Activity filterOutEnrolledStudent(Activity activity,Student student) {
        Long id = student.getId();
        for (int i = 0; i < activity.getEnrolledStudents().size(); i++) {
            if (activity.getEnrolledStudents().get(i).getId().equals(id)) {
                activity.getEnrolledStudents().remove(i);
                return activity;
            }
        }
        return activity;
    }

    @CrossOrigin
    @GetMapping("/activities/{id}/withdraw/{stdId}")
    public ResponseEntity withdrawActivity(@PathVariable("id") Long id, @PathVariable("stdId") Long stdId) {
        Activity activity = activityService.findById(id);
        List<Student> enrolledStudents = activity.getEnrolledStudents();
        Student stu = new Student();
        for (Student s : enrolledStudents ) {
            stu = s;
            if (stu.getId().equals( stdId )) {
                for (int i=0; i< stu.getEnrolledActivities().size();i++) {
                    if (stu.getEnrolledActivities().get(i).getId().equals(id)) {
                        stu.getEnrolledActivities().remove(i);
                        activity = filterOutEnrolledStudent(activity,stu);
                        activityService.saveActivity(activity);
                        studentService.saveStudent(stu);
                        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
                    }
                }

            }
        }
        activityService.saveActivity(activity);
        studentService.saveStudent(stu);
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
    }
    @CrossOrigin
    @GetMapping("/activities/teacher/{id}")
    public ResponseEntity getActivitiesByTeacherId(@PathVariable Long id) {
        List<ActivityDto> activityDtoList = MapperUtil.INSTANCE.getActivityDto(activityService.getAllActivities());
        List<ActivityDto> filteredActivityDtoList = new ArrayList<>();
        for (ActivityDto a : activityDtoList) {
            System.out.println("Activity's host ID: "+ a.getHost().getId() +" Provided ID: "+ id);
            if (a.getHost().getId().equals(id)) {
                filteredActivityDtoList.add(a);
            }
        }
        return ResponseEntity.ok(filteredActivityDtoList);
    }

}
