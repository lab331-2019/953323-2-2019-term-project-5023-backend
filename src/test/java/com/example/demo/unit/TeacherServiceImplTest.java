package com.example.demo.unit;

import com.example.demo.dao.StudentDao;
import com.example.demo.dao.TeacherDao;
import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.service.StudentServiceImpl;
import com.example.demo.service.TeacherServiceImpl;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Date;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TeacherServiceImplTest {
    @Mock
    TeacherDao teacherDao;
    @InjectMocks
    TeacherServiceImpl teacherService;

    static Teacher teacher1;
    static Teacher teacher2;
    static Teacher teacher3;
    @BeforeClass
    public static void setupTestData() {
        teacher1 = Teacher.builder()
                .name("Jayakrit")
                .surname("Hirisajja")
                .image(null)
                .id(1l)
                .build();
        teacher2 = Teacher.builder()
                .name("John")
                .surname("Smith")
                .image(null)
                .id(2l)
                .build();
        teacher3 = Teacher.builder()
                .name("Jane")
                .surname("Doe")
                .image(null)
                .id(3l)
                .build();
    }

    @Before
    public void setup() {
        //Set up the injected DAO
        ArrayList<Teacher> teachers = new ArrayList<>();
        teachers.add(teacher1);
        teachers.add(teacher2);
        teachers.add(teacher3);

        when(teacherDao.getAllTeachers()).thenReturn(teachers);
        when(teacherDao.findById(1l)).thenReturn(teacher1);
        when(teacherDao.findById(2l)).thenReturn(teacher2);
        when(teacherDao.findById(3l)).thenReturn(teacher3);
    }

    @Test
    public void testFindingTeacher() {
        //Test finding teacher
        assertThat(teacherService.getAllTeachers().size(),is(3));
        assertThat(teacherService.findById(1l),is(teacher1));
        assertThat(teacherService.findById(2l),is(teacher2));
        assertThat(teacherService.findById(3l),is(teacher3));
        assertThat(teacherService.findById(4l),nullValue());
    }

    @Test
    public void testDeletingTeacher() {
        //Test deleting teachers
        teacherService.deleteById(1l);
        verify(teacherDao,times(1)).deleteById(1l); //Check if teacher1 is removed once
        teacherService.deleteById(2l);
        verify(teacherDao,times(1)).deleteById(2l); //Check if teacher2 is removed once
    }

    @Test
    public void testAddingTeacher() {
        //Set up a new teacher list to test
        ArrayList<Teacher> teachers = new ArrayList<>();
        teachers.add(teacher1);
        teachers.add(teacher2);
        teachers.add(teacher3);
        Teacher teacher4 = Teacher.builder()
                .name("Killer")
                .surname("Queen")
                .image(null)
                .id(4l)
                .build();
        teachers.add(teacher4);
        when(teacherDao.getAllTeachers()).thenReturn(teachers);
        when(teacherDao.saveTeacher(teacher4)).thenReturn(teacher4);
        //Test Adding teacher
        Teacher newTeacher = teacherService.saveTeacher(teacher4);
        assertThat(teacherService.getAllTeachers(),hasItem(newTeacher));
        assertThat(teacherService.getAllTeachers(),hasItems(teacher1,teacher2,teacher3,newTeacher));
    }
}


